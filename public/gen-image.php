<?php

require_once '../config.php';
set_include_path('.:../lib');
require_once '../vendor/autoload.php';

require_once('get-avatar.php');
use Symfony\Component\Yaml\Yaml;

if (!in_array($_SERVER['REQUEST_METHOD'], ['GET', 'HEAD'])) {
  http_response_code(405);
  echo "Method now allowed.\n";
  die();
}

try {
  $available_templates = json_decode(file_get_contents('../public/tmpl-info.json'), true);
  if (
    !array_key_exists('template', $_GET) ||
    !in_array($_GET['template'], array_map(function($x) { return $x['handle']; }, $available_templates))
  ) {
    throw new RuntimeException('A valid template is required');
  }
  $template = $_GET['template'];
  $instructions = Yaml::parseFile('../public/tmpl-img/'.$template.'/instructions.yaml');

  if (!array_key_exists('avatar_source', $_GET)) {
    throw new RuntimeException('No avatar source is given');
  }
  switch ($_GET['avatar_source']) {
    case 'telegram':
      if (!array_key_exists('avatar_telegram_username', $_GET)) {
        throw new RuntimeException('Telegram username is not given');
      }
      $avatar_options = ['username' => $_GET['avatar_telegram_username']];
      break;
    case 'twitter':
      if (!array_key_exists('avatar_twitter_username', $_GET)) {
        throw new RuntimeException('Twitter username is not given');
      }
      $avatar_options = ['username' => $_GET['avatar_twitter_username']];
      break;
    default:
      $avatar_options = [];
  }
  $avatar = get_avatar($_GET['avatar_source'], $avatar_options);
  $canvas = new Imagick('../public/tmpl-img/'.$template.'/bg.png');

  foreach ($instructions as $inst) {
    if ($inst['file'] === 'avatar') {
      $sprite = new Imagick();
      $sprite->readImageBlob($avatar);
    } elseif (strpos($inst['file'], 'text:') === 0) {
      $sprite = new Imagick();
      $sprite->newImage(600, 600, 'transparent');
      $sprite_draw = new ImagickDraw();
      $sprite_draw->setFillColor('black');
      if (array_key_exists('font', $inst)) {
        $sprite_draw->setFont('../public/tmpl-img/'.$template.'/'.$inst['font']);
      }
      $sprite_draw->setFontSize($inst['font_size']);
      $sprite_draw->setTextAlignment(Imagick::ALIGN_LEFT);
      $text = substr($inst['file'], 5);
      $text = str_replace('${name}', @$avatar_options['username'] ?: 'OO', $text);
      $sprite_draw->annotation(0, $inst['font_size'], $text);
      $sprite->drawImage($sprite_draw);
    } else {
      $sprite = new Imagick('../public/tmpl-img/'.$template.'/'.$inst['file']);
    }

    $offset_x = 0;
    $offset_y = 0;

    if (array_key_exists('scale', $inst)) {
      $sprite->scaleImage($inst['scale'][0], $inst['scale'][1]);
    }
    if (array_key_exists('round_corner', $inst)) {
      //$sprite->roundCorners($inst['round_corner'], $inst['round_corner']);
      // roundCorners() no longer exists: https://github.com/Imagick/imagick/issues/213
      $mask = new Imagick();
      $mask->newImage($sprite->getImageWidth(), $sprite->getImageHeight(), 'transparent', 'png');
      $shape = new ImagickDraw();
      $shape->setFillColor('black');
      $shape->roundRectangle(0, 0, $sprite->getImageWidth() - 1, $sprite->getImageHeight() - 1, $inst['round_corner'], $inst['round_corner']);
      $mask->drawImage($shape);
      $sprite->setImageMatte(true);
      $sprite->compositeImage($mask, Imagick::COMPOSITE_DSTIN, 0, 0);
      $shape->clear();
      $mask->clear();
    }
    if (array_key_exists('rotate', $inst)) {
      $offset_x -= ($sprite->getImageWidth() / 2 - $sprite->getImageHeight() / 2 * tan(deg2rad($inst['rotate'] % 90) / 2)) * sin(deg2rad($inst['rotate'] % 90));
      $offset_y -= ($sprite->getImageHeight() / 2 - $sprite->getImageWidth() / 2 * tan(deg2rad($inst['rotate'] % 90) / 2)) * sin(deg2rad($inst['rotate'] % 90));
      $sprite->rotateImage('transparent', $inst['rotate']);
    }

    $canvas->compositeImage(
      $sprite,
      Imagick::COMPOSITE_OVER,
      $inst['position'][0] + round($offset_x), $inst['position'][1] + round($offset_y)
    );
  }

  if (array_key_exists('format', $_GET)) {
    $format = $_GET['format'];
  } else {
    $format = 'png';
  }
  switch ($format) {
    case 'jpeg':
      $canvas->setImageBackgroundColor('white');
      $canvas->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
      $canvas->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
      $canvas->setImageFormat('jpeg');
      $content_type = 'image/jpeg';
      break;
    case 'telegram-sticker':
      // Quote Telegram doc: dimensions must not exceed 512px, and either width or height must be exactly 512px
      $canvas->scaleImage(512, 512, true);
    case 'webp':
      $canvas->setImageFormat('webp');
      $content_type = 'image/webp';
      break;
    case 'png':
    default:
      $canvas->setImageFormat('png');
      $content_type = 'image/png';
  }

  header('Content-Type: '.$content_type);
  header('Content-Length: '.strlen($canvas));
  header('Cache-Control: max-age=300');

  echo $canvas;

} catch (RuntimeException $e) {
  http_response_code(400);
  header('Content-Type: text/plain; charset=utf-8');
  header('Cache-Control: no-cache');
  echo $e->getMessage().".\n";
}
