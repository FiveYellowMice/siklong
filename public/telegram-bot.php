<?php

require_once '../config.php';

function send_api_request($method, $params) {
  global $siklong_telegram_token;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_URL, 'https://api.telegram.org/bot'.$siklong_telegram_token.'/'.$method);
  curl_setopt($ch, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json; charset=utf-8' ]);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
  if (array_key_exists('http_proxy', $_SERVER)) {
    curl_setopt($ch, CURLOPT_PROXY, $_SERVER['http_proxy']);
  }
  $query = json_decode(curl_exec($ch), true);
  curl_close($ch);

  if (!$query) {
    return false;
  }

  if (!array_key_exists('result', $query)) {
    if (array_key_exists('description', $query)) {
      trigger_error('Telegram API error: '.$query['description']);
    }
    return false;
  }

  $result = $query['result'];
  return $result;
}

function process_message($update) {
  global $siklong_base_url;
  global $siklong_telegram_username;

  $message = $update['message'];

  if (
    array_key_exists('text', $message) &&
    (strpos($message['text'], '/eat_sticker ') === 0 ||
    strpos($message['text'], '/eat_sticker@'.$siklong_telegram_username.' ') === 0)
  ) {
    $args_str = implode(' ', array_slice(explode(' ', explode("\n", $message['text'])[0]), 1));

    $available_templates = json_decode(file_get_contents('../public/tmpl-info.json'), true);

    return [
      'method' => 'sendMessage',
      'chat_id' => $update['message']['chat']['id'],
      'text' => $args_str."\n\nChoose template:",
      'reply_markup' => [
        'inline_keyboard' => array_map(function($template) {
          global $siklong_base_url;
          return [[
            'text' => $template['name'],
            'callback_data' => $template['handle']
          ]];
        }, $available_templates)
      ],
    ];
  } elseif (
    $message['chat']['type'] === 'private' ||
    preg_match('/^\/help(?:@'.$siklong_telegram_username.')?(?: |$)/', @$message['text'])
  ) {
    return [
      'method' => 'sendMessage',
      'chat_id' => $message['chat']['id'],
      'parse_mode' => 'HTML',
      'text' => "Usage (inline): <code>@".$siklong_telegram_username." [telegram/twitter] username</code>\n".
        "Examples:\n".
        "<code>@".$siklong_telegram_username." durov</code>\n".
        "<code>@".$siklong_telegram_username." twitter realDonaldTrump</code>\n".
        "\n".
        "Usage (command): <code>/eat_sticker [telegram/twitter] username</code>\n".
        "Examples:\n".
        "<code>/eat_sticker durov</code>\n".
        "<code>/eat_sticker twitter realDonaldTrump</code>\n".
        "\n".
        "Web interface: ".$siklong_base_url
    ];
  } else {
    return null;
  }
}

function process_callback_query($update) {
  global $siklong_base_url;

  $callback = $update['callback_query'];

  if (!array_key_exists('message', $callback)) {
    return [
      'method' => 'answerCallbackQuery',
      'callback_query_id' => $callback['id'],
      'text' => 'Session expired.',
      'show_alert' => true,
    ];
  }

  $message = $callback['message'];
  $arguments = array_filter(explode(' ', explode("\n", $message['text'])[0]), function($s) {
    return !in_array($s, ['', null]);
  });

  if (sizeof($arguments) === 1) {
    $avatar_source = 'telegram';
    $username = $arguments[0];
  } elseif (sizeof($arguments) === 2) {
    $avatar_source = strtolower($arguments[0]);
    if (!in_array($avatar_source, ['telegram', 'twitter'])) {
      $avatar_source = 'telegram';
    }
    $username = $arguments[1];
  } else {
    return [
      'method' => 'answerCallbackQuery',
      'callback_query_id' => $callback['id'],
      'text' => 'Incorrect command argument.',
      'show_alert' => true,
    ];
  }
  if ($username[0] === '@') {
    $username = mb_substr($username, 1);
  }

  $available_templates = json_decode(file_get_contents('../public/tmpl-info.json'), true);

  if (!in_array($callback['data'], array_map(function($x) { return $x['handle']; }, $available_templates))) {
    return [
      'method' => 'answerCallbackQuery',
      'callback_query_id' => $callback['id'],
      'text' => 'Invalid template name.',
      'show_alert' => true,
    ];
  }

  $params = [
    'template' => $callback['data'],
    'avatar_source' => $avatar_source,
    'format' => 'telegram-sticker',
    'fc' => time(),
  ];
  switch ($avatar_source) {
    case 'telegram':
      $params['avatar_telegram_username'] = $username;
      break;
    case 'twitter':
      $params['avatar_twitter_username'] = $username;
      break;
  }
  $result_url = $siklong_base_url.'/gen-image.php?'.http_build_query($params);

  $send_result = send_api_request('sendSticker', [
    'chat_id' => $message['chat']['id'],
    'sticker' => $result_url,
  ]);

  if (!$send_result) {
    send_api_request('sendMessage', [
      'chat_id' => $message['chat']['id'],
      'text' => error_get_last()['message']."\n\nTry opening this URL instead:\n".$result_url,
    ]);
  }

  return [
    'method' => 'answerCallbackQuery',
    'callback_query_id' => $callback['id'],
  ];
}

function process_query($update) {
  global $siklong_base_url;

  $query_id = $update['inline_query']['id'];
  $arguments = array_filter(explode(' ', $update['inline_query']['query']), function($s) {
    return !in_array($s, ['', null]);
  });

  if (sizeof($arguments) === 1) {
    $avatar_source = 'telegram';
    $username = $arguments[0];
  } elseif (sizeof($arguments) === 2) {
    $avatar_source = strtolower($arguments[0]);
    if (!in_array($avatar_source, ['telegram', 'twitter'])) {
      $avatar_source = 'telegram';
    }
    $username = $arguments[1];
  } else {
    return [
      'method' => 'answerInlineQuery',
      'inline_query_id' => $query_id,
      'results' => [],
      'cache_time' => '0',
    ];
  }
  if ($username[0] === '@') {
    $username = mb_substr($username, 1);
  }

  $available_templates = json_decode(file_get_contents('../public/tmpl-info.json'), true);

  $results = array_map(function($template) use($username, $avatar_source) {
    global $siklong_base_url;

    $params = [
      'template' => $template['handle'],
      'avatar_source' => $avatar_source,
      'format' => 'jpeg',
      'fc' => time(),
    ];
    switch ($avatar_source) {
      case 'telegram':
        $params['avatar_telegram_username'] = $username;
        break;
      case 'twitter':
        $params['avatar_twitter_username'] = $username;
        break;
    }
    $result_url = $siklong_base_url.'/gen-image.php?'.http_build_query($params);

    return [
      'type' => 'photo',
      'photo_url' => $result_url,
      'thumb_url' => $siklong_base_url.'/tmpl-img/'.$template['handle'].'/bg.png',
      'photo_width' => $template['width'],
      'photo_height' => $template['height'],
      'title' => $template['name'],
      'id' => $template['handle'].'-'.strval(time()),
    ];
  }, $available_templates);

  return [
    'method' => 'answerInlineQuery',
    'inline_query_id' => $query_id,
    'results' => $results,
    'cache_time' => '0',
  ];
}

if (!(array_key_exists('token', $_GET) && $_GET['token'] == $siklong_webhook_token)) {
  http_response_code(403);
  echo 'Forbidden';
  die();
}

$update = json_decode(file_get_contents('php://input'), true);

if (!$update) {
  http_response_code(400);
  echo 'Not receiving proper JSON';
  die();
}

if (array_key_exists('message', $update)) {
  $response = process_message($update);
} elseif (array_key_exists('inline_query', $update)) {
  $response = process_query($update);
} elseif (array_key_exists('callback_query', $update)) {
  $response = process_callback_query($update);
} else {
  http_response_code(204);
  die();
}

http_response_code(204);
if ($response) {
  send_api_request($response['method'], $response);
}
