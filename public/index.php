<?php

require_once '../config.php';

if (!in_array($_SERVER['REQUEST_METHOD'], ['GET', 'HEAD'])) {
  http_response_code(405);
  echo "Method now allowed.\n";
  die();
}

?><!DOCTYPE html>
<html lang="en">
  <head prefix="og: http://ogp.me/ns#">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Siklong</title>
    <link rel="canonical" href="<?= $siklong_base_url ?>/">
    <meta name="description" content="Generate images that eat avatars.">

    <meta property="og:title" content="Siklong">
    <meta property="og:site_name" content="Siklong">
    <meta property="og:description" content="Generate images that eat avatars.">
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?= $siklong_base_url ?>/">
    <meta property="twitter:card" content="summary">
    <meta property="twitter:site" content="@FiveYellowMice">

    <link rel="stylesheet" href="<?= $siklong_base_path ?>/node_modules/bootstrap/dist/css/bootstrap.min.css?fc=piRveGx">
  </head>
  <body>
    <div id="siklong-controls" class="container mb-4">
      <h1 class="text-center mt-3 mb-4">Siklong</h1>
      <p>Generate images that eat avatars.</p>
      <p><a href="https://gitlab.com/FiveYellowMice/siklong">Source Code</a> | <a href="https://t.me/eat_avatar_bot">Telegram Bot</a></p>
      <div class="row">
        <div class="col-12 col-xl-9">
          <div class="card mt-3">
            <div class="card-header"><span class="mr-2">🌭</span>Choose Avatar</div>
            <div class="card-body">
              <ul class="nav nav-pills mb-3">
                <li class="nav-item"><a class="nav-link" :class="{'active': avatarSource === 'telegram'}" role="button" :aria-pressed="avatarSource === 'telegram'" v-on:click.prevent="avatarSource = 'telegram'" href="#">Telegram</a></li>
                <li class="nav-item"><a class="nav-link" :class="{'active': avatarSource === 'twitter'}" role="button" :aria-pressed="avatarSource === 'twitter'" v-on:click.prevent="avatarSource = 'twitter'" href="#">Twitter</a></li>
              </ul>
              <div v-show="avatarSource === 'telegram'" class="form-group">
                <label for="avatar-telegram-username">Telegram Username</label>
                <input id="avatar-telegram-username" v-model="avatarTelegramUsername" type="text" class="form-control">
              </div>
              <div v-show="avatarSource === 'twitter'" class="form-group">
                <label for="avatar-twitter-username">Twitter Username</label>
                <input id="avatar-twitter-username" v-model="avatarTwitterUsername" type="text" class="form-control">
              </div>
            </div>
          </div>
          <div class="card mt-3">
            <div class="card-header"><span class="mr-2">🍽️</span>Choose Template</div>
            <div class="card-body">
              <div class="row">
                <div v-for="template in templates" class="col-lg-3 col-md-4 col-sm-6 col-12 mb-3">
                  <button class="btn btn-block btn-outline-primary h-100" :class="{'active': templateSelection === template.handle}" :aria-pressed="templateSelection === template.handle" v-on:click="templateSelection = template.handle">
                    <div class="d-flex align-items-start flex-column h-100">
                      <div class="my-auto w-100 text-center">
                        <img class="mw-100" :src="'<?= $siklong_base_path ?>/tmpl-img/'+template.handle+'/bg.png'" :alt="template.name">
                      </div>
                      <div class="mt-2 w-100 text-center">{{ template.name }}</div>
                    </div>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-xl-3">
          <div class="card mt-3">
            <div class="card-header"><span class="mr-2">😋</span>Result</div>
            <div class="card-body">
              <div class="mb-3">
                <button type="submit" class="btn btn-success" :disabled="showProgress" v-on:click.prevent="generateResult">Generate</button>
              </div>
              <div class="progress mb-3" v-show="showProgress">
                <div class="progress-bar progress-bar-striped progress-bar-animated w-100" role="progressbar"></div>
              </div>
              <p v-if="resultMessage">{{ resultMessage }}</p>
              <p v-if="resultUrl" class="text-center"><img class="mw-100" :src="resultUrl"></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer class="text-center text-secondary mt-4 mb-2">
      Made with &#x1f60b; by <a href="https://fym.moe/">FiveYellowMice</a>
    </footer>

    <script>
    document.addEventListener('DOMContentLoaded', function() {
      var siklongControls = new Vue({
        el: '#siklong-controls',
        data: {
          avatarSource: 'telegram',
          avatarTelegramUsername: '',
          avatarTwitterUsername: '',
          templates: [],
          templateSelection: null,
          generateButtonClicked: false,
          resultMessage: null,
          resultUrl: null
        },
        computed: {
          showProgress: function() {
            return this.generateButtonClicked && !this.resultMessage && !this.resultUrl
          }
        },
        methods: {
          generateResult: function() {
            try {
              this.generateButtonClicked = true
              if (this.resultUrl) URL.revokeObjectURL(this.resultUrl)
              this.resultUrl = null
              this.resultMessage = null
              var params = new URLSearchParams({
                avatar_source: this.avatarSource,
                template: this.templateSelection,
                format: 'png',
                fc: Math.floor(Date.now() / 1000)
              })
              switch (this.avatarSource) {
                case 'telegram':
                  if (!this.avatarTelegramUsername) {
                    throw new Error('Telegram username is not given')
                  }
                  params.set('avatar_telegram_username', this.avatarTelegramUsername)
                  break
                case 'twitter':
                  if (!this.avatarTwitterUsername) {
                    throw new Error('Twitter username is not given')
                  }
                  params.set('avatar_twitter_username', this.avatarTwitterUsername)
                  break
                default:
                  throw new Error('Invalid avatar source')
              }
              if (!this.templateSelection) {
                throw new Error('Please select a template')
              }
              fetch("<?= $siklong_base_path ?>/gen-image.php?" + params.toString())
              .then((response) => {
                if (!response.ok) {
                  return response.text().then(text => Promise.reject(text))
                }
                return response.blob()
              }).then((blob) => {
                this.resultUrl = URL.createObjectURL(blob)
              }).catch((message) => {
                this.resultMessage = message || 'Could not generate image.'
              })
            } catch (err) {
              this.resultMessage = err.message + '.'
            }
          }
        }
      })
      fetch('<?= $siklong_base_path ?>/tmpl-info.json')
      .then(result => result.json())
      .then((result) => {
        siklongControls.templates = result
      })
    })
    </script>

    <script src="<?= $siklong_base_path ?>/node_modules/vue/dist/vue.min.js?fc=piRveGx"></script>
  </body>
</html>
