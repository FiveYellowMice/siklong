<?php

function get_avatar($source, $options) {
  switch ($source) {
    case 'telegram':
      if (
        !array_key_exists('username', $options) ||
        !is_string($options['username']) ||
        !preg_match('/^[A-Za-z0-9_]{5,}$/', $options['username'])
      ) {
        throw new RuntimeException('Telegram user name is not provided or invalid');
      }
      $ch = curl_init();
      curl_setopt_array($ch, [
        CURLOPT_URL => 'https://t.me/'.$options['username'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_PROTOCOLS => CURLPROTO_HTTPS
      ]);
      $res = curl_exec($ch);
      if ($res === false) {
        throw new RuntimeException('Could not get avatar: curl: '.curl_error($ch));
      }
      if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
        throw new RuntimeException('Could not get avatar: received HTTP status '.curl_getinfo($ch, CURLINFO_HTTP_CODE));
      }
      curl_close($ch);
      $doc = new DOMDocument();
      if (!@$doc->loadHTML($res)) {
        throw new RuntimeException('Could not get avatar: unable to parse HTML');
      }
      $xpath = new DOMXPath($doc);
      $url = $xpath->query("//img[contains(@class, 'tgme_page_photo_image')]/@src")->item(0)->value;
      if (strlen($url) === 0) {
        $page_icon = $xpath->query("//div[contains(@class, 'tgme_page_icon')]")->item(0);
        if ($page_icon) {
          // When user does not exist, t.me returns 200, and shows no avatar but a chat bubble icon and text as if the user exists. So when an icon exists, it is a known behaviour rather than a parse failure.
          throw new RuntimeException('Could not get avater: user does not exist');
        } else {
          throw new RuntimeException('Could not get avatar: could not get image URL');
        }
      }
      if (!str_starts_with($url, 'https:')) {
        throw new RuntimeException('Could not get avatar: user does not have an avatar image');
      }
      $img = file_get_contents($url);
      if ($img === false) {
        throw new RuntimeException('Could not get avatar: failed to load image URL');
      }
      return $img;
      break;
    case 'twitter':
      if (
        !array_key_exists('username', $options) ||
        !is_string($options['username']) ||
        !preg_match('/^[A-Za-z0-9_]{5,}$/', $options['username'])
      ) {
        throw new RuntimeException('Twitter user name is not provided or invalid');
      }
      $ch = curl_init();
      curl_setopt_array($ch, [
        CURLOPT_URL => 'https://twitter.com/@'.$options['username'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_PROTOCOLS => CURLPROTO_HTTPS
      ]);
      $res = curl_exec($ch);
      if ($res === false) {
        throw new RuntimeException('Could not get avatar: curl: '.curl_error($ch));
      }
      if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
        throw new RuntimeException('Could not get avatar: received HTTP status '.curl_getinfo($ch, CURLINFO_HTTP_CODE));
      }
      curl_close($ch);
      $doc = new DOMDocument();
      if (!@$doc->loadHTML($res)) {
        throw new RuntimeException('Could not get avatar: unable to parse HTML');
      }
      $xpath = new DOMXPath($doc);
      $url = $xpath->query("//img[contains(@class, 'ProfileAvatar-image')]/@src")->item(0)->value;
      if (strlen($url) === 0) {
        throw new RuntimeException('Could not get avatar: could not get image URL');
      }
      $img = file_get_contents($url);
      if ($img === false) {
        throw new RuntimeException('Could not get avatar: loading image failed');
      }
      return $img;
      break;
    default:
      throw new RuntimeException('Invalid avatar source');
  }
}
